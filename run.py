from flask import Flask, render_template, request, flash, redirect, url_for
from werkzeug import secure_filename
from models import db,transactions
from util.GenericUtilities import load_data_from_file,process_messages, extract_attributes

import os

app = Flask(__name__)

##  Setting the upload folder to uploads
app.config['UPLOAD_FOLDER'] = 'uploads'
## Setting the max uploadable file length to 10 MB
app.config['MAX_CONTENT_PATH'] =  10485760
## Secret key for csrf security and enabling flashing messages
app.config['SECRET_KEY'] =  'mysecretkey1234567'
## Fetching the DB location
DB_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'storage/debit_card.db')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(DB_PATH)
## Disabling the DB modification tracking
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

@app.route('/upload')
def upload_page():
    return render_template('upload.html')

@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        ## The posted file is first copied automatically to a temp location
        ## The file name can be retrieved by request.files['file'].filename
        ## However it is a recommended to retrieve it using the function secure_filename()

        file_location = request.files['file']
        upload_location_string = "/".join([app.config['UPLOAD_FOLDER'], secure_filename(file_location.filename)])
        file_location.save(upload_location_string)
        # flash('file uploaded successfully')
        # return render_template('upload.html')
        return redirect(url_for('persist', file=upload_location_string))

@app.route('/persist')
def persist():
    file_name = request.args.get('file')

    ## Fetching data from json and loading into a dictionary
    messages = load_data_from_file(file_name)
    ## Filtering the appropriate messages
    messages = process_messages(messages)

    for( transaction_id, sender, card_number, transaction_amount, transaction_datetime, sms_datetime) in extract_attributes(messages):
        print "Sender: " + sender
        print "Debit Card Number: " + card_number
        print "Transaction Datetime: " + str(transaction_datetime)
        print "Transaction Amount: " + str(transaction_amount)
        print "Message Datetime: " + str(sms_datetime)
        ## Adding to database
        db.session.add(transactions(transaction_id, sender, card_number, transaction_amount, transaction_datetime, sms_datetime))
        # db.session.commit()
    db.session.commit()

    flash('file uploaded and records persisted successfully')
    return render_template('upload.html')

@app.route('/dashboard')
def dashboard():
    records = transactions.query.all()
    return render_template('dashboard.html', transactions=records)


if __name__ == '__main__':
    with app.app_context():
        db.init_app(app)
        db.create_all()
        app.run(debug=True)