This project is a simple flask project for uploading a json file containing the message dump from my handset and filtering and loading the data
into a simple db and displaying them on a simple dashboard

The dump retrieved from the Windows mobile is an XML file
Converted the xml dump from Windows to json on

 https://codebeautify.org/xmltojson

Important Notes:

The application is written with python2.7 using flask framework

The timestamp in the messages.json is windows timestamp.The following formula is used to convert it to Unix Timestamp
(windows_timestamp / (10*1000*1000))-11644473600

The conditions written to filter the messages are mostly generic but may not be exhaustive for every kind of message dump.
However they are good enough for the given data set

If the implementation has to be generic, use an NLP model or some open source NLP engine for the code to learn over a period of time

The schema of the data dump anticipated by this code should be as follows

{
	"ArrayOfMessage": {
		"Message": [
			{
				"Recepients": "",
				"Body": "<Message Body>",
				"IsIncoming": "true",
				"IsRead": "<true/false>",
				"Attachments": "",
				"LocalTimestamp": "<windows_timestamp>",
				"Sender": "<sender>"
			},....
			       ]
			         }
}

data folder contains the dump of the messages I have extracted. It is the same file I have used to test this code.

util directory contains codes for general utilities

The apis given by this project are

/upload  --> The url from which the file could be uploaded. The uploaded file goes to uploads directory
             and the contents of the file are written to DB

/dashboard --> Displays the data extracted from messages in a tabular format

The login mechanism is not implemented. There would be a few changes in certain methods and data model if it has to be implemented
The dashboard is a plain vanilla dashboard without any filters and sorts

UUIDs are used for transaction IDs

Debug is set to true for this app

Instructions for testing:

Install the modules mentioned in requirements.txt using the following command
pip install -r requirements.txt

Run the following command to initiate the server
python run.py

Then use the following url to go to upload page
http://localhost:5000/upload