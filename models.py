from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class transactions(db.Model):
    id                 = db.Column('transaction_id', db.String(100), primary_key = True)
    sender             = db.Column(db.String(50))
    card_number        = db.Column(db.String(20))
    transaction_amount = db.Column(db.String(20))
    transaction_date   = db.Column(db.String(50))
    sms_datetime       = db.Column(db.String(50))

    def __init__(self, transaction_id, sender, card_number, transaction_amount, transaction_date, sms_datetime):
        self.id = transaction_id
        self.sender = sender
        self.card_number = card_number
        self.transaction_amount = transaction_amount
        self.transaction_date = transaction_date
        self.sms_datetime = sms_datetime