import json
import re
import datetime
import uuid

def load_data_from_file(file):

    ## Declaring a dictionary for holding messages data
    messages = json.load(open(file))
    message_list = messages['ArrayOfMessage']["Message"]
    return message_list

def process_messages(message_list):

    ## debit_card_re_pattern = '([0-9]*)(x*|X*)([0-9]{4})'
    match_patterns = [r'card', r'([0-9]*)(x*|X*)([0-9]{4})', r'( rs+| inr+|^rs+|^inr+)']

    match_patterns_fun = lambda string,arr: True if len([x for x in arr if not re.search(x,string,re.IGNORECASE) == None]) == len(arr) else False
    match_fun = lambda s: match_patterns_fun(s["Body"].lower(),match_patterns)

    stop_message_patterns = [
                             ['^Mobile/DTH/Data', 'card', 'recharge','Citibank', 'online','A/C'],
                             ['incorrect','PIN','thrice','blocked'],
                             ['^hurry','great','chance','win'],
                             ['withdrawn','at','atm'],
                             ['^Dear Customer','long','customer','queues','delays','servicing','request'],
                             ['^From','enjoy','cashback','upto','movie','bookings']
                            ]
    stop_message_for_pattern_fun = lambda string,pattern: True if len([x for x in pattern if not re.search(x,string,re.IGNORECASE) == None]) == len(pattern) else False
    stop_message_fun = lambda s:True if len([x for x in stop_message_patterns if stop_message_for_pattern_fun(s["Body"].lower(),x)]) == 0 else False

    stop_senders = ['RM-144555',
                    'DM-FCTZEN',
                    'VK-040801',
                    'IM-001125',
                    'DM-152484',
                    'VM-040801',
                    'IM-001126',
                    'TM-Citibk',
                    'ID-ENCRCL',
                    'TD-610303',
                    'HP-IAPPLE',
                    'TX-AxisBk',
                    'DZ-CRSWRD',
                    'DM-300838',
                    'VM-MOBKAR',
                    'DM-MOBKAR',
                    'VK-MOBKAR',
                    'DM-088686',
                    'DM-AXISBK',
                    'DM-020001',
                    'DM-IDFCBK',
                    'DM-098067',
                    'AD-WCKDRD',
                    'AD-MTROBK',
                    'DZ-SSFRST',
                    'DM-CRSWRD',
                    'VK-040726',
                    'VM-040722',
                    'IM-AXISBK',
                    'AD-017253',
                    'DM-366000',
                    'DM-200846']

    ## Filtering Messages

    ## Filtering messages by match patterns
    filtered_messages = filter(match_fun,message_list)
    ## Filtering messages by stop patterns
    filtered_messages = filter(stop_message_fun,filtered_messages)
    ## Filtering messages by senders
    filtered_messages = [message for message in filtered_messages if message["Sender"] not in stop_senders]

    return filtered_messages

def extract_attributes(messages):

    dc_patterns = [r'([0-9]+)(x+|X+)([0-9]{4})',
                   r'([0-9]*)(x+|X+)([0-9]{4})',
                   r'([0-9]*)(x*|X*)(\*+)([0-9]{4})',
                   r'([0-9]{4})(x*|X*)(\**)([0-9]{4})',
                   r'card.([0-9]{4})',
                   r'([0-9]*)(x*|X*)([0-9]{4})']
    date_pattern = [r'([0-9]{2})-([A-Z]{3})-([0-9]{2})']
    amount_pattern = [r'rs.( {0,1})(\d+)(\.{0,1})(\d*)',r'inr.( {0,1})(\d+)(\.{0,1})(\d*)']

    for x in messages:
        # print "Message: " + x["Body"]
        # print "Sender: " + x["Sender"]
        # print "Debit Card Number: " + str(extract_data(x["Body"],dc_patterns,empty_reducer))
        # print "Transaction Date: " + str(extract_data(x["Body"], date_pattern,hiphen_reducer))
        # print "Transaction Amount: " + str(extract_data(x["Body"], amount_pattern,empty_reducer))
        # print "Transaction Datetime: " + str(get_message_date(x))

        transaction_id = uuid.uuid1().hex
        sender         = x["Sender"]
        card_number    = 'XX' + str(extract_data(x["Body"],dc_patterns,empty_reducer))[-4:]
        transaction_amount = str(extract_data(x["Body"], amount_pattern,empty_reducer))
        sms_datetime   = str(get_message_date(x))
        transaction_datetime   = str(extract_data(x["Body"], date_pattern,hiphen_reducer)).title() if not extract_data(x["Body"], date_pattern,hiphen_reducer) == None else sms_datetime

        yield ( transaction_id, sender, card_number, transaction_amount, transaction_datetime, sms_datetime)

reducer_lambda = lambda x,y,z:z.join([x,y])
empty_reducer = lambda x,y:reducer_lambda(x,y,'')
hiphen_reducer = lambda x,y:reducer_lambda(x,y,'-')

def extract_data(string, patterns,reducer):
    for pattern in patterns:
        match = re.search(pattern,string,re.IGNORECASE)
        if match:
            return reduce(reducer,list(re.findall(pattern,string,re.IGNORECASE)[0])).strip(' ')

def get_message_date(message):
    unix_timestamp = (int(message['LocalTimestamp'])/10000000)-11644473600
    timestamp = datetime.datetime.fromtimestamp(unix_timestamp).strftime('%d-%b-%Y %H:%M:%S')
    return timestamp


if __name__ == "__main__":
    load_data_from_file()